CREATE USER IF NOT EXISTS myuser@'%' IDENTIFIED BY 'password';

CREATE DATABASE IF NOT EXISTS mydb;
GRANT ALL ON mydb.* TO myuser@'%';
FLUSH PRIVILEGES;
