#!/bin/bash

# Set environment variables from .env file
export $(grep -v '^#' .env-dev | xargs)

# Change docker-compose.yaml file
CURRENT_USER=$(id -un)
CURRENT_USERID=$(id -u)
sed -i "s/CURRENT_USER.*/CURRENT_USER=$CURRENT_USER;/" .env-dev
sed -i "s/CURRENT_USERID.*/CURRENT_USERID=$CURRENT_USERID;/" .env-dev

# Change nginx.conf file
sed -i "s/server_name .*/server_name $APP_URL;/" dev/nginx/default.conf

# Remove DB folder, if
if [ -d dev/db ]; then
  read -rp "Remove DataBase ? [y/N]: "
  if [[ "$REPLY" == [Yy]* ]]; then
      echo 'Remove DataBase ...'
      sudo rm -rf dev/db
  else
      echo 'Skip remove'
  fi
fi

# Create DB and user
sed -i "s/myuser/$DB_USERNAME/" dev/sql/initdb.sql
sed -i "s/password/$DB_PASSWORD/" dev/sql/initdb.sql
sed -i "s/mydb/$DB_DATABASE/" dev/sql/initdb.sql

# Change permissions for docker
echo "Change permissions..."
sudo chown -R "$CURRENT_USER":82 .
docker compose up -d

# Install Laravel
if [ ! -d vendor ]; then
    echo "Installing Laravel..."
#    docker exec "$APP_SLUG" composer update
    docker exec $APP_SLUG composer create-project laravel/laravel lara
    mv lara/{,.}* .
    rm -r lara
    sed -i "s/APP_NAME=.*/APP_NAME=$APP_NAME/" .env
    sed -i "s/APP_URL=.*/APP_URL=$APP_URL/" .env
    sed -i "s/DB_CONNECTION=.*/DB_CONNECTION=$DB_CONNECTION/" .env
    sed -i "s/DB_HOST=.*/DB_HOST=$DB_HOST/" .env
    sed -i "s/DB_PORT=.*/DB_PORT=$DB_PORT/" .env
    sed -i "s/DB_DATABASE=.*/DB_DATABASE=$DB_DATABASE/" .env
    sed -i "s/DB_USERNAME=.*/DB_USERNAME=$DB_USERNAME/" .env
    sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=$DB_PASSWORD/" .env
    sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=$DB_PASSWORD/" .env
    sed -i "s/REDIS_HOST=.*/REDIS_HOST=redis/" .env

    docker exec $APP_SLUG composer require laravel/ui
    docker exec $APP_SLUG php artisan ui bootstrap --auth
    docker exec $APP_SLUG php artisan migrate
    if [ -n "$(grep -P "SANCTUM_STATEFUL_DOMAINS" .env)" ]; then
      sed -i "s/SANCTUM_STATEFUL_DOMAINS=.*/SANCTUM_STATEFUL_DOMAINS=$APP_URL,localhost:3000,localhost:8080/" .env
    else
      echo "SANCTUM_STATEFUL_DOMAINS=$APP_URL,localhost:3000,localhost:8080/" >> .env
    fi

#    Configure vue
    mv vite.config.js-dev vite.config.js
    mv app.js-dev resources/js/app.js
#    Create example
    mkdir -p resources/js/views
    mv example.vue-dev resources/js/views/example.vue
    sed -i "s/@endsection/<p>\&nbsp;<\/p><example><\/example>@endsection/" resources/views/auth/login.blade.php
#    Install npm
    npm i vue@next @vitejs/plugin-vue --save-dev
    npm install && npm run build
fi

# Generate key
docker exec "$APP_SLUG" php artisan key:generate

# Add Certificate SSL
mkdir dev/certs
mkcert -cert-file dev/certs/ssl_certificate.pem -key-file dev/certs/ssl_certificate_key.pem "$APP_URL" "*.$APP_URL" localhost 127.0.0.1 ::1

# Add url to /etc/hosts file
if [ -n "$(grep -P "$APP_URL" /etc/hosts)" ]; then
  echo "Site $APP_URL already exists in hosts"
else
  echo "Adding in hosts:"
  echo "127.0.0.1 $APP_URL" | sudo tee -a /etc/hosts
fi

echo 'Change permission with excluded dev'
sudo chown -R "$CURRENT_USER":82 $(ls -I dev)
sudo chmod -R 777 storage bootstrap

echo ""
echo "Web ready on:" "http://$APP_URL" and "https://$APP_URL"
echo "npm run dev"
