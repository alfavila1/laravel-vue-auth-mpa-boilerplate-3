# Laravel+Sanctum+Vue+Bootstrap - MPA Boilerplate
### This Boilerplate has:
* Laravel last + Sanctum
* Vue 3
* Bootstrap 5
* Login, Register, ... Views

### How to use this template:
* `git clone ... myapp`
* `cd myapp`
* Edit `.env-dev`
* Run `./setup.sh` to configure and install Laravel, Vue and example
